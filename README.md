# VB Kanye

## Starting up Gitlab Runner
- Download from https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-386.exe
- Create directory C:/Gitlab-Runner
- Rename executable to gitlab-runner.exe
- Move executable into directory
- Go into Powershell with Admin rights and type `./gitlab-runner.exe register`
- Fill in registration with your token (from Gitlab in browser) and set executor type as Shell
- Type './gitlab-runner.exe install'
- Start the runner with './gitlab-runner.exe start'

### Running gitlab-ci.yaml locally
- Add C:\Gitlab-Runner to path
- 'gitlab-runner exec shell build_job'