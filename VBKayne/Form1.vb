﻿Imports System.Net.Http
Public Class Form1
    Private Async Sub GetQuote_ClickAsync(sender As Object, e As EventArgs) Handles Btn_GetQuote.Click
        Dim url As String = "https://api.kanye.rest/"
        Using client As HttpClient = New HttpClient()
            Using response As HttpResponseMessage = Await client.GetAsync(url)
                Using content As HttpContent = response.Content
                    Dim quoteResponse As QuoteResponse = Await content.ReadAsAsync(Of QuoteResponse)()
                    Lbl_Quote.Text = quoteResponse.Quote
                End Using
            End Using
        End Using
    End Sub
End Class

Public Class QuoteResponse
    Public Property Quote As String
End Class